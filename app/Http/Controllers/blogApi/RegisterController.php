<?php

namespace App\Http\Controllers\blogApi;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|max:255|min:3',
            'firstname' => 'max:255|min:3',
            'lastname' => 'max:255|min:3',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone_number' => 'numeric|regex:/(0)[0-9]{8}/',
            'birthday'=>'',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function register(Request $req)
    {
        $avatarName = '';
        //$this->validator($req->all())->validate();
        $error = $this->validator($req->all())->errors();
        //$error = json_decode($error);
        $response=$error->messages();
        if(!empty($error->messages())){
            return response()->json($response, 500);
        }
        if(isset($req->avatar)){
            $avatarName = $this->avatarUpload($req->avatar);
            
         }
         if(empty($req->birthday)){
             $req->birthday = null;
         }
         
        User::create([
            'username' => $req->username,
            'firstname' => $req->firstname,
            'lastname' => $req->lastname,
            'phone_number' => $req->phone_number,
            'birthday' => $req->birthday,
            'avatar' => $avatarName,
            'email' => $req->email,
            'password' => bcrypt($req->password),
        ]);
        return response()->json(['ok'],200);
    }
    private function avatarUpload($data)
    {
    	//dd($data->getClientOriginalExtension());
        $imageName = time().'.'.$data->getClientOriginalExtension();
        $data->move(public_path('avatar'), $imageName);
    	return $imageName;
    }
}
