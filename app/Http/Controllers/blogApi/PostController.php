<?php

namespace App\Http\Controllers\blogApi;

use Illuminate\Http\Request;
use App\Category;
use App\Posts;
use Validator;
use Carbon\Carbon;
use Auth;
use App\Http\Controllers\Controller;


class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    protected function validator(array $data,$params)
    {
        return Validator::make($data, $params);
    }
    public function show($id)
    {
        $post=Posts::find($id);
        return response()->json(compact('post'),200);
    
    }
    public function update(Request $request,$id)
    {
        $imagename = Posts::whereId($id)->pluck('avatar')[0];
        $this->validate($request,[
            'title' => 'required|max:255|min:3',
            'description' => 'required|min:3',
            'category' => 'required|numeric',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        
        $data=$request->all();
        $post = Posts::find($id);
        if($request->hasFile('avatar')){
            $image = $request->file('avatar');
            $imagename = time() . str_random(5).'.'.$image->getClientOriginalExtension();
            $image->move(public_path() . '/avatar/', $imagename);
            //var_dump($image);die;
            $data['image']=$imagename;
        }
        
       
        $post->update(['title'=>$request->title,'avatar'=>$imagename,'description'=>$request->description,'categories_id'=>$request->category]);
            return response()->json( ['message' => 'success'],200);
     }
    public function getAddPost(Category $category)
    {
        $data = $category->get()->toArray();
        return view('posts.index')->with('categ',$data);
        
    }
    public function store(Request $request)
    {
        
        $params= [
            'title' => 'required|max:255|min:3',
            'description' => 'required|min:3',
            'category' => 'required|numeric',
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
        $this->validator($request->all(),$params)->validate();
        $current = Carbon::now();
        $avatarName = '';
        if(isset($request->avatar)){
            $avatarName = $this->avatarUpload($request->avatar);
        }
        Posts::create([
            'title' => $request->title,
            'description' => $request->description,
            'user_id' => Auth::user()->id,
            'categories_id' => $request->category,
            'publish_date' =>$current,
            'avatar' => $avatarName,
        ]);
        return response()->json(compact('ok'),201);
        
        
    }
    public function destroy($id)
    {
        $oldAvatar = Posts::whereId($id)->pluck('avatar');
        if(Posts::whereId($id)->delete()){
            unlink(public_path('avatar'.'/'.$oldAvatar['0']));
        };
        return response()->json( ['message' => 'success'],200);
        
    }
    
    private function avatarUpload($data)
    {
    	$imageName = time().'.'.$data->getClientOriginalExtension();
        $data->move(public_path('avatar'), $imageName);
    	return $imageName;
    }
    
    
}