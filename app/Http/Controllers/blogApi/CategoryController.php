<?php
namespace App\Http\Controllers\blogApi;
use App\Category;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;



class CategoryController  extends Controller{
    public function __construct()
    {
       $this->middleware('auth');
    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|max:255|min:3',
            'description' => 'required|min:3',
        ]);
    }
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
    public function index(Category $category)
    {
        $category = $category->all();
        return response()->json(compact('category'),200);

    }
    public function store(Category $category, Request $request)
    {
        
        $data=$request->all();
       
        $error = $this->validator($request->all())->errors();
        //$error = json_decode($error);
        $response=$error->messages();
        if(!empty($error->messages())){
            return response()->json($response, 500);
        }
        $data['user_id'] = Auth::user()->id;
        $category->fill($data);
        $category->save();
        return response()->json(compact('ok'),201);
        
        
    }    
    public function getCategoryPosts(Category $category,$id)
    {
        
        $selected = $category::whereId($id)->first();
        $posts = $category::find($id)->post()->whereCategories_id($id)->get();
        return response()->json(compact('posts','selected'),200);
    }

}
